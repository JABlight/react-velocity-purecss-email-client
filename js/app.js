'use strict';

var EmailApp = require('./react_components/EmailApp.react');

var React = require('react/addons');

React.renderComponent(
    EmailApp(null),
    document.getElementById('emailapp')
);