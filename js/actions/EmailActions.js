'use strict';

var AppDispatcher = require('../dispatcher/AppDispatcher');
var EmailConstants = require('../constants/EmailConstants');

var EmailActions = {

    create: function (name, subject, description) {
        AppDispatcher.handleViewAction({
            actionType: EmailConstants.EMAIL_CREATE,
            name: name,
            subject: subject,
            description: description
        });
    },

    destroy: function (id) {
        AppDispatcher.handleViewAction({
            actionType:EmailConstants.EMAIL_DESTROY,
            id: id
        })
    },

    select: function (id) {
        AppDispatcher.handleViewAction({
            actionType: EmailConstants.EMAIL_SELECT,
            id: id
        });
    }
};

module.exports = EmailActions;