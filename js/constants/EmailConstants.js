'use strict';

var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({
    EMAIL_CREATE: null,
    EMAIL_SEND: null,
    EMAIL_DESTROY: null,
    EMAIL_SELECT: null
});