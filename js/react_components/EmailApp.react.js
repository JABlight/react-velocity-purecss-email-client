'use strict';

var Nav = require('./Nav.react.js');
var List = require('./List.react.js');
var Main = require('./Main.react.js');

var EmailStore = require('../stores/EmailStore');
var React = require('react/addons');

function getEmailState() {
    return {
        allEmails: EmailStore.getAll()
    };
}

var EmailApp = React.createClass({

    getInitialState: function () {
        return getEmailState();
    },

    componentDidMount: function () {
        EmailStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        EmailStore.removeChangeListener(this.onChange);
    },

    /**
     * @return {Object}
     */
    render: function () {
        return (
            React.DOM.div({
                id: 'layout',
                className: 'content pure-g'
            },
                Nav(null),
                List({
                    allEmails: this.state.allEmails
                }),
                Main(null)
            )
        );

    },

    onChange: function () {
        this.setState(getEmailState());
    }

});

module.exports = EmailApp;