'use strict';

var ListItem = require('./ListItem.react.js');
var EmailActions = require('../actions/EmailActions');

var Velocity = require('velocity-animate');
var React = require('react/addons');
var ReactTransitionGroup = React.addons.TransitionGroup;


var List = React.createClass({

    getDefaultProps: function () {
        return {
            allEmails: []
        };
    },

    getInitialState: function () {
        return {
            selectedEmailId: 1
        };
    },

    componentDidMount: function () {
        var listItems = document.querySelectorAll('.email-item');

        // Animate the items
        Velocity( listItems, 'transition.fadeIn', { stagger: 100, delay: 1200} );
        // Animate the list
        Velocity( this.getDOMNode(), { opacity: [1, 0] }, { delay: 800 });
    },

    /**
     * @return {object}
     */
    render: function () {
        var emails = [],        // Stores the email React components to render
            key;                // Used in for loop to iterate over all emails

        var allEmails = this.props.allEmails;

        for (key in allEmails) {
            if (allEmails.hasOwnProperty(key)) {
                emails.push(
                    ListItem({
                        key: key,
                        email: allEmails[key],
                        selectedEmailId: this.state.selectedEmailId,
                        onOpen: this.onOpen,
                        onDelete: this.onDelete
                    })
                );
            }
        }

        return (
            React.DOM.div({
                id: 'list',
                className: 'pure-u-1'
            },
                ReactTransitionGroup({
                    component: React.DOM.div
                },
                    emails
                )
            )
        );
    },

    onOpen: function (emailId) {
        EmailActions.select(emailId);

        this.setState({
            selectedEmailId: emailId
        });
    },

    onDelete: function (emailId) {
        EmailActions.destroy(emailId);

        if (this.state.selectedEmailId === emailId) {
            this.onOpen(null);
        }
    }
});

module.exports = List;