'use strict';

var Velocity = require('velocity-animate');
require('velocity-animate/velocity.ui');

var React = require('react/addons');

var MainContent = React.createClass({

    componentWillEnter: function(cb) {
        Velocity(this.getDOMNode(), 'transition.expandIn', { duration: 300, begin: cb });
        //cb();
    },

    /**
     * @return {Object}
     */
    render: function () {
        var email = this.props.email;

        return (
            React.DOM.div({
                    className: 'email-content'
                },
                React.DOM.div({
                        className: 'email-content-header pure-g'
                    },
                    React.DOM.div({
                            className: 'pure-u-1-2'
                        },
                        React.DOM.h1({
                            className: 'email-content-title'
                        }, email.subject),
                        React.DOM.p({
                            className: 'email-content-subtitle'
                        }, 'From ' + email.name + ' at 3:56pm, April 3, 2012')
                    ),
                    React.DOM.div({
                            className: 'email-content-controls pure-u-1-2'
                        },
                        React.DOM.button({
                            className: 'secondary-button pure-button'
                        }, 'Reply'),
                        React.DOM.button({
                            className: 'secondary-button pure-button'
                        }, 'Forward'),
                        React.DOM.button({
                            className: 'secondary-button pure-button'
                        }, 'Move To')
                    )
                ),
                React.DOM.div({
                        className: 'email-content-body'
                    },
                    React.DOM.p(null,
                        email.desc
                    )

                )
            )
        );
    }
});

module.exports = MainContent;