'use strict';

var Velocity = require('velocity-animate');
require('velocity-animate/velocity.ui');

var React = require('react/addons');

// Constants
var selectedZIndex = 10;

var ListItem = React.createClass({

    propTypes: {
        email: React.PropTypes.object.isRequired,
        onOpen: React.PropTypes.func.isRequired
    },

    componentDidUpdate: function (prevProps) {
        var animateNode = this.getDOMNode();

        // Animate this item if it was just selected
        if (this.props.selectedEmailId === this.props.email.id) {
            if (prevProps.selectedEmailId !== this.props.email.id) {
                // Set Z index so the box shadow shows above all elements
                animateNode.style.zIndex = selectedZIndex;
                // Run the animation
                Velocity(animateNode, {boxShadowBlur: 10}, {duration: 200});
            }
        } else {
            // Remove box shadow as this item is not selected
            animateNode.style.boxShadow = '';
            // Reset the zIndex value
            animateNode.style.zIndex = '';
        }
    },

    componentWillLeave: function (cb) {
      Velocity(this.getDOMNode(), 'transition.expandOut', {duration: 200, complete: cb});
    },

    /**
     * @return {Object}
     */
    render: function () {

        var email = this.props.email,
            selected = this.props.selectedEmailId === email.id;

        var cx = React.addons.classSet;
        var emailClasses = cx({
            'email-item': true,
            'pure-g': true,
            'email-item-unread': email.unread,
            'email-item-selected': selected
        });

        return (
            React.DOM.div({
                className: emailClasses,
                onClick: this._onOpenClick.bind(this, email.id)
            },
                React.DOM.div({
                        className: 'pure-u'
                    },
                    React.DOM.img({
                        className: 'email-avatar',
                        height: 64,
                        width: 64,
                        src: email.icon
                    })
                ),
                React.DOM.div({
                        className: 'pure-u-3-4'
                    },
                    React.DOM.h5({
                        className: 'email-name'
                    }, email.name ),
                    React.DOM.h4({
                        className: 'email-subject'
                    }, email.subject ),
                    React.DOM.p({
                        className: 'email-desc'
                    }, email.desc ),
                    React.DOM.button({
                        className: 'pure-button button-xsmall button-error',
                        onClick: this._onDeleteClick.bind(this, email.id)
                    }, 'delete')
                )
            )
        );
    },

    _onOpenClick: function (emailId) {
        this.props.onOpen(emailId);
    },

    _onDeleteClick: function (emailId, e) {
        e.stopPropagation();
        this.props.onDelete(emailId);
    }
});

module.exports = ListItem;