'use strict';

var Velocity = require('velocity-animate');
require('velocity-animate/velocity.ui');

var NavMenu = require('./NavMenu.react');

var React = require('react/addons');

var Nav = React.createClass({

    componentDidMount: function () {

        var seq = [
            { elements: this.getDOMNode(),
                properties: {
                    height: ['100%', 0],
                    opacity: 1
                },
                options: {
                    delay: 100,
                    display: 'block'
                }
            },
            { elements: this.getDOMNode(), properties: 'callout.bounce'}
        ];

        // Animate compose button
        Velocity(this.refs.composebutton.getDOMNode(), 'transition.fadeIn', { delay: 900 });
        // Animate nav menu
        Velocity.RunSequence(seq);
    },

    /**
     * @return {object}
     */
    render: function () {
        return (
            React.DOM.div({
                id: 'nav',
                className: 'pure-u'
            },
                React.DOM.a({
                    className: 'nav-menu-button'
                }, 'Menu'),
                React.DOM.div({
                    className: 'nav-inner'
                },
                    React.DOM.button({
                        className:'primary-button pure-button',
                        ref: 'composebutton'
                    }, 'Compose'),
                    NavMenu(null)
                )
            )
        );
    }
});

module.exports = Nav;