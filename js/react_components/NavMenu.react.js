'use strict';

var EmailStore = require('../stores/EmailStore');
var React = require('react/addons');
var Velocity = require('velocity-animate');
require('velocity-animate/velocity.ui');

function getNavState() {
    return {
        unreadCount: EmailStore.getUnreadCount()
    };
}

var NavMenu = React.createClass({

    /**
     * @returns {object}
     */
    getInitialState: function () {
        return getNavState();
    },

    componentDidMount: function () {
        // Items to animate
        var items = document.querySelectorAll('.pure-menu li');

        // Entry animation
        Velocity(items, 'transition.slideLeftIn', { stagger: 100, delay: 900 });

        EmailStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        EmailStore.removeChangeListener(this._onChange);
    },

    //componentWillUpdate: function (nextProps, nextState) {
    //    var countNode = this.refs.InboxEmailCount.getDOMNode();
    //    // If the unread count has changed then animate the change
    //    if (nextState.unreadCount !== this.state.unreadCount) {
    //        Velocity(countNode, 'transition.expandOut');
    //    }
    //},

    componentDidUpdate: function (nextProps, nextState) {
        var countNode = this.refs.InboxEmailCount.getDOMNode();
        // If the unread count has changed then animate the change
        if (nextState.unreadCount !== this.state.unreadCount) {
            Velocity(countNode, 'transition.slideDownIn', {duration: 250});
        }
    },

    /**
     * @return {Object}
     */
    render: function () {
        var unreadCount = this.state.unreadCount

        function getNavItem(item) {
            return (
                React.DOM.li({
                        key: 'nav-' + item
                    },
                    React.DOM.a({
                            href: '#/' + item
                        }, item,
                        React.DOM.span({
                            className: 'email-count',
                            ref: item + 'EmailCount'
                        },
                            item === 'Inbox' ?
                                ' (' + unreadCount + ')' :
                                ''
                        )
                    )
                )
            );
        }

        return (
            React.DOM.div({
                className: 'pure-menu pure-menu-open'
            },
                React.DOM.ul(null,
                    getNavItem('Inbox'),
                    getNavItem('Important'),
                    getNavItem('Sent'),
                    getNavItem('Drafts'),
                    getNavItem('Trash')
                )
            )
        );
    },

    _onChange: function () {
        this.setState({ unreadCount: EmailStore.getUnreadCount() });
    }
});

module.exports = NavMenu;