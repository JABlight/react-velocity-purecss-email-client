'use strict';

var EmailStore = require('../stores/EmailStore');
var MainContent = require('./MainContent.react');

var Velocity = require('velocity-animate');
require('velocity-animate/velocity.ui');

var React = require('react/addons');
var ReactTransitionGroup = React.addons.TransitionGroup;

function getEmailState () {
    return {
        email: EmailStore.getSelected()
    };
}

var Main = React.createClass({

    /**
     * @return {object}
     */
    componentDidMount: function () {
        // Entry animation
        Velocity(this.getDOMNode(), 'transition.expandIn', { duration: 300, delay: 1200 });

        // Listen for email selection event
        EmailStore.addChangeListener(this._onEmailSelected);
    },

    componentWillUnmount: function () {
        // Cleanup any attached listeners
        EmailStore.removeChangeListener(this._onEmailSelected);
    },

    /**
     * @return {object}
     */
    getInitialState: function () {
        return getEmailState();
    },

    /**
     * @return {object}
     */
    render: function () {

        var email = this.state.email;

        return (
            ReactTransitionGroup({
                component: React.DOM.div,
                id: 'main',
                className: 'pure-u-1'
            },
                email ?
                MainContent({
                    email: email,
                    key: 'email-' + email.id
                }) :
                    null
            )
        );
    },

    _onEmailSelected: function () {
        this.setState(getEmailState());
    }
});

module.exports = Main;