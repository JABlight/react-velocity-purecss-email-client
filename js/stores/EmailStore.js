'use strict';

var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var EmailConstants = require('../constants/EmailConstants');
var merge = require('react/lib/merge');

var CHANGE_EVENT = 'change';

// All emails
var _emails = {
    // Dummy data
    1: {
        id: 1,
        name: 'Tilo Mitra',
        subject: 'Hello from Toronto',
        desc: 'Hey, I just wanted to check in with you from Toronto. I got here earlier today.',
        icon: 'img/common/tilo-avatar.png',
        unread: false
    },
    2: {
        id: 2,
        name: 'Eric Ferraiulo',
        subject: 'Re: Pull Requests',
        desc: 'Hey, I had some feedback for pull request #51. We should center the menu so it looks better on mobile.',
        icon: 'img/common/ericf-avatar.png',
        unread: true
    },
    3: {
        id: 3,
        name: 'YUI Library',
        subject: 'You have 5 bugs assigned to you',
        desc: 'Duis aute irure dolor in reprehenderit in voluptate velit essecillum dolore eu f',
        icon: 'img/common/yui-avatar.png',
        unread: true
    },
    4: {
        id: 4,
        name: 'Reid Burke',
        subject: 'Re: Design Language',
        desc: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa.',
        icon: 'img/common/reid-avatar.png',
        unread: true
    },
    5: {
        id: 5,
        name: 'Andrew Wooldridge',
        subject: 'YUI Blog Updates?',
        desc: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.',
        icon: 'img/common/andrew-avatar.png',
        unread: true
    },
    6: {
        id: 6,
        name: 'Yahoo! Finance',
        subject: 'How to protect your finances from winter storms',
        desc: 'Mauris tempor mi vitae sem aliquet pharetra. Fusce in dui purus, nec malesuada mauris.',
        icon: 'img/common/yfinance-avatar.png',
        unread: false
    },
    7: {
        id: 7,
        name: 'Yahoo! News',
        subject: 'Summary for April 3rd, 2012',
        desc: 'We found 10 news articles that you may like.',
        icon: 'img/common/ynews-avatar.png',
        unread: false
    }
};

// Keeps track of which email is selected (open)
var _selectedEmailId = 1;          // Default selected email

/**
 * Create an email
 * @param name {String}     The email name
 * @param subject {String}  The email subject
 * @param description {String}     The email description
 */
function create(id, name, subject, description) {
    var id = (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
    _emails[id] = {
        id: id,
        name: name,
        subject: subject,
        description: description,
        icon: null,
        unread: true
    };
}

function destroy (id) {
    delete _emails[id];
}

/**
 * Mark an email as read
 * @param id
 */
function select (id) {
    if (id) {
        _emails[id].unread = false;
    }
}

var EmailStore = merge(EventEmitter.prototype, {

    /**
     * Get the collection of emails
     * @returns {object}
     */
    getAll: function () {
        return _emails;
    },

    /**
     * get the selected email Id
     */
    getSelected: function () {
        return _emails[_selectedEmailId];
    },

    getUnreadCount: function () {
        var count = 0,
            k;

        for ( k in _emails ) {
            if (_emails[k].unread) {
                count += 1;
            }
        };

        return count;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

// Register to handle all updates
AppDispatcher.register(function (payload){
    var action = payload.action;
    var name, subject, description;

    switch(action.actionType) {
        case EmailConstants.EMAIL_CREATE:
            name = action.name.trim();
            subject = action.subject.trim();
            description = action.description.trim();

            if (name !== '') {
                create(name, subject, description);
            }
            break;

        case EmailConstants.EMAIL_DESTROY:
            destroy(action.id);
            break;

        case EmailConstants.EMAIL_SELECT:
            _selectedEmailId = action.id;
            select(_selectedEmailId);
            break;

        default:
            return true;
    }

    EmailStore.emitChange();

    return true; // No errors. Needed by promise in Dispatcher
});

module.exports = EmailStore;