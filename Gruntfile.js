module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        watch: {
            react: {
                files: 'js/**/*.js',
                tasks: ['browserify']
            }
        },

        browserify: {
            client: {
                src: ['js/**/*.js'],
                dest: 'dist/app.built.js'
            },
            options: {
                browserifyOptions: {
                    debug: true
                }
            }
        },

        uglify: {
            build: {
                src: 'dist/app.built.js',
                dest: 'dist/app.built.min.js'
            }
        }
    });

    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', [
        'browserify',
        'uglify'
    ]);
};