## An animated version of the [static](http://purecss.io/layouts/email/) Pure css email client
### Uses React for the UI, Velocity for animation and Pure for CSS

### [Live Demo](http://jablight.bitbucket.org/projects/pvr-email-client/)